<?php

if(!isset($_GET["rating"])){
    header("Location: search.php");
}

$rating = $_GET["rating"];

$host = 'itp460.usc.edu';
$dbname = 'dvd';
$user = 'student';
$pass = 'ttrojan';

$pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);

$sql = "SELECT dvds.title, genres.genre_name, formats.format_name, ratings.rating_name
        FROM dvds
        INNER JOIN genres
        ON dvds.genre_id = genres.id
        INNER JOIN formats
        ON dvds.format_id = formats.id
        INNER JOIN ratings
        ON dvds.rating_id = ratings.id
        WHERE ratings.rating_name = '$rating'
        ORDER BY dvds.title ASC";

$statement = $pdo->prepare($sql);

$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_OBJ);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Search Results | Title Search</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js" />
    <style>
    .result:nth-child(even), .empty {
        background-color: #EEEEEE;
    }

    .header-line {
        font-weight: bold;
        background-color: lightsteelblue;
    }

    .title {
        text-align: center;
    }

    .back-button {
        line-height: 40px;
    }

    </style>
</head>
<body>
    <div class="container">
        <div class="content">
            <div class="row title">
                <h3 class="col-md-2 text-center back-button"><a href="search.php">< Back</a></h3>
                <h1 class="col-md-8 text-center">You searched for all movies rated <em><?php echo $rating ?></em></h1>
            </div>
            <?php if(count($results) > 0) :?>
                <div class="row header-line">
                    <div class="col-md-5">DVD Title</div>
                    <div class="col-md-3">Genre</div>
                    <div class="col-md-2">Format</div>
                    <div class="col-md-2">Rating</div>
                </div>
                <?php foreach($results as $dvd) : ?>
                    <div class="row result">
                        <div class="col-md-5"><?php echo $dvd->title ?></div>
                        <div class="col-md-3"><?php echo $dvd->genre_name ?></div>
                        <div class="col-md-2"><?php echo $dvd->format_name ?></div>
                        <div class="col-md-2"><a href="ratings.php?rating=<?php echo $dvd->rating_name ?> "><?php echo $dvd->rating_name ?></a></div>
                    </div>
                <?php endforeach; ?>
            <?php else : ?>
                <div class="row">
                    <div class="col-md-12 empty text-center">No results to show. Please go <a href="search.php">here</a> to search again.</div>
                </div>

            <?php endif; ?>



        </div>
    </div>
</body>
</html>
