<!DOCTYPE html>
<html>
    <head>
        <title>DVD Database Search -- PDO</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js" />
        <style>
            .content {
                width: 1000px;
                margin: 100px auto;
            }

            textarea:focus, input:focus{
                outline: none;
            }

            input {
                outline: none;
                border: none;
                font-size: 36pt;
            }

            input[type=text] {
                border-bottom: thin solid black;
            }

            input[type=submit] {
                font-size: 24pt;
                color: white;
            }

            input[type=submit]:hover {
                color: #CCCCCC;
                background-color: #333;
            }

            #search_form {
                margin-top: 100px;
            }

            .title {
                color: black;
                font-size: 28pt;
            }


        </style>
    </head>
    <body>
        <div class="container">

            <div class="content text-center">
                <div class="title">Search our dvd collection</div>

                <form id="search_form" action="results.php" method="get">
                    <input type="text" name="title" value="" placeholder="Enter search term here">
                    <input type="submit" value="Search">
                </form>

            </div>
        </div>
    </body>
</html>
